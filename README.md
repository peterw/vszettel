# vszettel README

VSZettel is an extension to add enhance markdown files to make them better for
making and using a zettelkasten system of notetaking.

## Features

1. Autocomplete wiki-links, and go-to definition
2. Autocomplete tags
3. Create new note command
4. Create new note command that inserts the new filename into your current
   editor. With this approach, you can just make links and navigate forwards
   without needing to find the file you just created and make a link to it.

## Todo

- [ ] Autocompleting tags probably has bad performance for a large number of
      files, I should look into that.

## Developing

In VSCode, start the webpack development mode watch task with
`yarn webpack-dev`. This will keep the code compiling, and you can then use
`<F5>` to launch the debugger.

## Credits

I was super inspired by
[VSCode Markdown Notes](https://github.com/kortina/vscode-markdown-notes) and
I've lifted some ideas and code from that repo, notably the syntax highlighting.
I also pulled the "Create Note" command straight from
[VSNotes](https://github.com/patleeman/VSNotes), along with the related
settings. I chose to copy files instead of fork because I also wanted to move to
typescript, and it was easier to convert _just_ the stuff I needed.
