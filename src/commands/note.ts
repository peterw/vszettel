import * as vscode from "vscode";
import { ensureFile } from "fs-extra";
import * as path from "path";
import * as moment from "moment";
import { resolveHome } from "../utils";

/*
Copied from https://github.com/patleeman/VSNotes/blob/master/src/newNote.js
*/

type Token = {
  token: string;
  type: string;
  format: string;
};

type BeforeShowNote = (noteName: string) => void;

// This function handles creation of a new note in default note folder
export function newNote() {
  const config = vscode.workspace.getConfiguration("vszettel");
  const noteFolder = resolveHome(config.get<string>("defaultNotePath"));
  const templates = config.get<string[]>("templates");

  if (!templates || !templates.length) {
    createNote({ noteFolder });
    return;
  }

  vscode.window
    .showQuickPick(templates, {
      placeHolder: "Please select a template. Hit esc to use default."
    })
    .then(
      template => {
        console.log(template);
        createNote({ noteFolder, template });
      },
      err => {
        console.error(err);
      }
    );
}

export function newDailyJournalNote() {
  const config = vscode.workspace.getConfiguration("vszettel");
  const noteFolder = resolveHome(config.get<string>("defaultNotePath"));
  const templates = config.get<string[]>("templates");
  const tokens = config.get<Token[]>("tokens", []);

  const journalNoteTitle = config.get<string>(
    "journalNoteTitle",
    "Daily Journal {short_date}"
  );

  const processedJournalNoteTitle = replaceTokens(
    journalNoteTitle,
    "Untitled",
    tokens
  );

  if (!templates || !templates.length) {
    createNote({ noteFolder, initialNoteName: processedJournalNoteTitle });
    return;
  }

  vscode.window
    .showQuickPick(templates, {
      placeHolder: "Please select a template. Hit esc to use default."
    })
    .then(
      template => {
        console.log(template);
        createNote({
          noteFolder,
          template,
          initialNoteName: processedJournalNoteTitle
        });
      },
      err => {
        console.error(err);
      }
    );
}

export function newNoteWithLink(textEditor: vscode.TextEditor) {
  const config = vscode.workspace.getConfiguration("vszettel");
  const noteFolder = resolveHome(config.get<string>("defaultNotePath"));
  const templates = config.get<string[]>("templates");

  const insertLink = (url: string) => {
    textEditor.insertSnippet(new vscode.SnippetString(url));
  };

  if (!templates || !templates.length) {
    createNote({ noteFolder, onBeforeShowNote: insertLink });
    return;
  }

  vscode.window
    .showQuickPick(templates, {
      placeHolder: "Please select a template. Hit esc to use default."
    })
    .then(
      template => {
        console.log(template);
        createNote({ noteFolder, template });
      },
      err => {
        console.error(err);
      }
    );
}

async function createNote({
  noteFolder,
  template,
  onBeforeShowNote,
  initialNoteName
}: {
  noteFolder?: string | null;
  template?: string;
  onBeforeShowNote?: BeforeShowNote;
  initialNoteName?: string | null;
}) {
  const config = vscode.workspace.getConfiguration("vszettel");
  const defaultNoteName = config.get<string>("defaultNoteName");
  const tokens = config.get<Token[]>("tokens", []);
  const noteTitleConvertSpaces = config.get<string>(
    "noteTitleConvertSpaces",
    "_"
  );

  let noteTitle = config.get<string>("defaultNoteTitle", "{title}.{ext}");

  if (noteFolder === null || !noteFolder) {
    vscode.window.showErrorMessage(
      "Default note folder not found. Please run setup."
    );
    return;
  }

  // Get the name for the note
  const inputBoxPromise = vscode.window.showInputBox({
    prompt: `Note title? Current Format ${noteTitle}. Hit enter for instant note.`,
    value: initialNoteName || ""
  });

  inputBoxPromise.then(
    noteName => {
      // Check for aborting the new note dialog
      if (noteName === null) {
        return false;
      }

      // Check for empty string but confirmation in the new note dialog
      if (!noteName || noteName === "") {
        noteName = defaultNoteName;
      }

      let fileName = replaceTokens(noteTitle, noteName!, tokens);

      if (noteTitleConvertSpaces !== null) {
        fileName = fileName.replace(/\s/g, noteTitleConvertSpaces);
      }

      // Create the file
      const createFilePromise = createFile(noteFolder, fileName);
      createFilePromise.then(filePath => {
        if (typeof filePath !== "string") {
          console.error("Invalid file path");
          return false;
        }

        if (onBeforeShowNote) {
          onBeforeShowNote(path.basename(filePath));
        }

        vscode.window
          .showTextDocument(vscode.Uri.file(filePath), {
            preserveFocus: false,
            preview: false
          })
          .then(() => {
            console.log("Note created successfully: ", filePath);

            createTemplate({ template });
          });
      });
    },
    err => {
      vscode.window.showErrorMessage("Error occurred while creating note.");
      console.error(err);
    }
  );
}

function createTemplate({ template = null }: { template?: string | null }) {
  const config = vscode.workspace.getConfiguration("vszettel");

  if (template !== null) {
    vscode.commands
      .executeCommand(
        "editor.action.insertSnippet",
        ...[{ langId: "markdown", name: `vsnote_template_${template}` }]
      )
      .then(
        res => {
          vscode.window.showInformationMessage(
            `Note for "${template}" created!`
          );
          console.log("template created: ", res);
        },
        err => {
          vscode.window.showErrorMessage("Template creation error.");
          console.error("template creation error: ", err);
        }
      );
  } else {
    // default template
    const snippetLangId = config.get("defaultSnippet.langId");
    const snippetName = config.get("defaultSnippet.name");

    // Insert the default note text
    if (snippetLangId !== null && snippetName !== null) {
      vscode.commands
        .executeCommand(
          "editor.action.insertSnippet",
          ...[{ langId: snippetLangId, name: snippetName }]
        )
        .then(
          res => {
            console.log(res);
          },
          err => {
            console.error(err);
          }
        );
    }
  }
}

// Create the given file if it doesn't exist
function createFile(folderPath: string, fileName: string) {
  return new Promise((resolve, reject) => {
    if (folderPath === null || fileName === null) {
      reject();
    }
    const fullPath = path.join(folderPath, fileName);
    // fs-extra
    ensureFile(fullPath)
      .then(() => {
        resolve(fullPath);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function replaceTokens(format: string, title: string, tokens: Token[]) {
  let newFormat = format;
  const pattern = /(?:\{)(.+?)(?:\})/g;
  let result;
  while ((result = pattern.exec(format)) !== null) {
    for (let token of tokens) {
      if (token.token === result[0]) {
        switch (token.type) {
          case "datetime":
            newFormat = newFormat.replace(
              new RegExp(result[0], "g"),
              moment().format(token.format)
            );
            break;
          case "title":
            let prependedPath: string[] = [];
            // Check if its a nested path
            const splitTitle = title.split(path.sep);
            if (splitTitle.length > 1) {
              title = splitTitle[splitTitle.length - 1];
              prependedPath = splitTitle.slice(0, splitTitle.length - 1);
            }
            newFormat = prependedPath
              .concat(newFormat.replace(new RegExp(token.token, "g"), title))
              .join(path.sep);
            break;
          case "extension":
            newFormat = newFormat.replace(
              new RegExp(token.token, "g"),
              token.format
            );
            break;
        }
      }
    }
  }
  return newFormat;
}
