/**
 * wikilinkStartRegex matches `[[` and `[[startofsearch`.  Its useful because if
 * you hit `ctrl+space` to open the autocomplete menu, matching the preceding
 * `[[` will help this extension realize "we're starting to type a link"
 */
export const wikilinkStartRegex = /(\[\[){1}([\.\w_-])*/;

/**
 * Matches the entire wikilink, with both opening and closing brackets required.
 * It also must contain a word.
 */
export const wikilinkRegex = /\[\[([\.\w_-])+\]\]/;

/**
 * `wikilinkUrlRegex` matches the stuff on the inside of the brackets of a valid
 * wikilink.  The following are examples of supported wikilinks: `[[1234]]`
 * `[[1234.md]]` `[[1234_some_title.md]]` `[[other_note]]` `[[other_node.md]]`
 */
export const wikilinkUrlRegex = /[\.\w_-]+/;

/**
 * Same as `wikilinkUrlRegex` but the word could be empty.  This is useful for
 * matching just the opening brackets and determing we're about to start typing
 * a link.
 */
export const wikilinkTextOrEmpty = /[\.\w_-]?/;

/**
 * I only want to link to .md files
 */
export const linkableFileRegex = /\.md/i;

/**
 * Matches the word inside a tag
 */
export const tagWordRegex = /[\w]*/;

/**
 * Matches a complete tag
 */
export const tagRegex = /\#[\w]+/;

/**
 * Matches all tags in a file
 */

export const tagSearchRegex = /\#([\w]+)/g;

/**
 * Matches the start of a tag, so it could just be '#'
 */
export const tagStartRegex = /\#[\w]*/;
