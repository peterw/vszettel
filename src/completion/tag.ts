import * as vscode from "vscode";
import {
  linkableFileRegex,
  tagStartRegex,
  tagWordRegex,
  tagSearchRegex
} from "./regexes";
import { TextDecoder } from "util";
import { resolveHome, union } from "../utils";

/**
 * TODO: this will scan all remote files as well, maybe thats not good?
 * @param path A Uri for a given file to get tags from
 */
const getTagsInFile = async (path: vscode.Uri): Promise<Set<string>> => {
  const uintArray = await vscode.workspace.fs.readFile(path);

  const decoded = new TextDecoder().decode(uintArray);

  const matches = decoded.match(tagSearchRegex);

  if (!matches) {
    return new Set();
  }

  return new Set(matches.map(m => m.trim().replace("#", "")));
};

const getAllTags = async (folderPath: string): Promise<Array<string>> => {
  const filesInFolder = await vscode.workspace.findFiles(
    `**/*.md`,
    "**/node_modules/**"
  );

  const linkableFiles = filesInFolder.filter(f => {
    return f.scheme === "file" && f.path.match(linkableFileRegex);
  });

  const promises = linkableFiles.map(async u => await getTagsInFile(u));
  return Promise.all(promises).then(sets => {
    const tags = sets.reduce((acc, currentSet) => union(acc, currentSet));
    return Array.from(tags.values());
  });
};

export class TagCompletionItemProvider
  implements vscode.CompletionItemProvider {
  async provideCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    token: vscode.CancellationToken,
    context: vscode.CompletionContext
  ) {
    const range = document.getWordRangeAtPosition(position, tagStartRegex);
    if (!range) {
      return null;
    }
    const tagWithPound = document.getText(range);
    const tagTextMatches = tagWordRegex.exec(tagWithPound);
    if (tagTextMatches === null) {
      return;
    }
    // At this point, we know we're in a "tag" completion context

    const config = vscode.workspace.getConfiguration("vszettel");
    const noteFolder = resolveHome(config.get<string>("defaultNotePath"));

    const tags = await getAllTags(noteFolder);
    return tags.map(
      t => new vscode.CompletionItem(t, vscode.CompletionItemKind.Keyword)
    );
  }
}
