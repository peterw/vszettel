import * as vscode from "vscode";
import { basename } from "path";
import {
  linkableFileRegex,
  wikilinkStartRegex,
  wikilinkRegex,
  wikilinkTextOrEmpty,
  wikilinkUrlRegex
} from "./regexes";

// TODO: update link regex to accept other things that are OK in a filename, like '-'

const getLinkableFiles = async () => {
  return (await vscode.workspace.findFiles("**/*.md")).filter(f => {
    return f.scheme === "file" && f.path.match(linkableFileRegex);
  });
};

const findFilesMatchingLink = async (wikilink: string) => {
  const files = await getLinkableFiles();

  return files.filter(f => {
    return basename(f.path).startsWith(wikilink);
  });
};

export class WikiLinkCompletionItemProvider
  implements vscode.CompletionItemProvider {
  async provideCompletionItems(
    document: vscode.TextDocument,
    position: vscode.Position,
    token: vscode.CancellationToken,
    context: vscode.CompletionContext
  ) {
    const range = document.getWordRangeAtPosition(position, wikilinkStartRegex);
    if (!range) {
      return null;
    }
    const linkWithBrackets = document.getText(range);
    const linkTextMatches = wikilinkTextOrEmpty.exec(linkWithBrackets);
    if (linkTextMatches === null) {
      return;
    }
    // At this point, we know we're in a "wikilink" completion context

    const files = await getLinkableFiles();
    return files.map(f => {
      let kind = vscode.CompletionItemKind.File;
      const label = basename(f.path);
      return new vscode.CompletionItem(label, kind);
    });
  }
}

/**
 * This definition will match the first file that starts with the "word" inside
 * of the wikilink.  It might not be the right one, but who knows!
 */
export class WikiLinkDefinitionProvider implements vscode.DefinitionProvider {
  public async provideDefinition(
    document: vscode.TextDocument,
    position: vscode.Position,
    token: vscode.CancellationToken
  ) {
    const range = document.getWordRangeAtPosition(position, wikilinkRegex);
    if (!range) {
      return null;
    }
    const linkWithBrackets = document.getText(range);
    const linkTextMatches = wikilinkUrlRegex.exec(linkWithBrackets);
    if (!linkTextMatches) {
      return null;
    }
    // At this point, we know we've got some valid link text!

    const wikilink = linkTextMatches[0];
    const fileMatches = await findFilesMatchingLink(wikilink);

    if (fileMatches.length > 0) {
      return new vscode.Location(fileMatches[0], new vscode.Position(0, 0));
    }

    return null;
  }
}
