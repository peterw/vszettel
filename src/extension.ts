// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import {
  WikiLinkCompletionItemProvider,
  WikiLinkDefinitionProvider
} from "./completion/wikilink";
import { TagCompletionItemProvider } from "./completion/tag";
import { newNote, newNoteWithLink, newDailyJournalNote } from "./commands/note";

export function activate(context: vscode.ExtensionContext) {
  console.log('Congratulations, your extension "vszettel" is now active!');

  const markdownFiles = { scheme: "file", language: "markdown" };
  vscode.languages.setLanguageConfiguration("markdown", {
    wordPattern: /([\.\/\\\w_]+)/
  });

  // Create a new note
  let newNoteDisposable = vscode.commands.registerCommand(
    "vszettel.newNote",
    newNote
  );
  context.subscriptions.push(newNoteDisposable);

  let newNoteWithLinkDisposable = vscode.commands.registerTextEditorCommand(
    "vszettel.newNoteWithLink",
    newNoteWithLink
  );
  context.subscriptions.push(newNoteWithLinkDisposable);

  let newDailyJournalNoteDisposable = vscode.commands.registerTextEditorCommand(
    "vszettel.newDailyJournalNote",
    newDailyJournalNote
  );
  context.subscriptions.push(newDailyJournalNoteDisposable);

  context.subscriptions.push(
    vscode.languages.registerCompletionItemProvider(
      markdownFiles,
      new WikiLinkCompletionItemProvider()
    )
  );
  context.subscriptions.push(
    vscode.languages.registerDefinitionProvider(
      markdownFiles,
      new WikiLinkDefinitionProvider()
    )
  );

  context.subscriptions.push(
    vscode.languages.registerCompletionItemProvider(
      markdownFiles,
      new TagCompletionItemProvider()
    )
  );
}

export function deactivate() {}
